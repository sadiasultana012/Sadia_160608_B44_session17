<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/25/2017
 * Time: 1:04 PM
 */

namespace App\Utility;


class Utility
{
    public static function d($data){
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }

}