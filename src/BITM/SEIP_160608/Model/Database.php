<?php
namespace App\Model;
use PDO;
use PDOException;

class Database
{
    public $DBH;

    //localhost = 127.0.0.1
    public function __construct()
    {

        try {
            $this->DBH = new PDO('mysql:host=localhost;dbname=atomic_project_b44', "root", "");
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

            echo "Database Connection Successful" . "<br>";

        } catch (PDOException $error) {
            print "Database Error! : " . $error->getMessage() . "<br/>";
            die();
        }
    }

}